# Express CRUD - json file 
## TS based express rest API

It implements a users router and handle incoming requests to **Create**, **Update** and **Delete** a user.
It also allow us to **Read** a single user and return a **list** of all existing users.
Our DB is a **JSON file**.
It also implements a **log plain text file** that logs each request made to the server in a single line and  includes the request http **method**, the **path** and a **timestamp**.   

