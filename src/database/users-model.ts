import fs from "fs/promises";
import { usersList } from "../types/types";

class UsersModel {
    dbFilePath: string = "";
    usersLst: usersList = [];
    
    async init(dbFilePath: string) {
        this.dbFilePath = dbFilePath;

        let file = await fs.open(this.dbFilePath,"a");
        const stat = await file.stat();

        if (stat.size > 0) {
            //file already exists. parse it and return its content
            await this.load()
        } else {
            //create the file with empty array(default),
            await this.save();
        }
    }

    async load() {
        const json = await fs.readFile(this.dbFilePath, "utf-8");
        this.usersLst = JSON.parse(json);
    }

    async save(usersList: usersList = []){
        await fs.writeFile(this.dbFilePath, JSON.stringify(usersList, null, 2));
    }
}

const instance = new UsersModel();
export default instance;