import { NextFunction, Request, Response } from "express";
import fs from "fs/promises";
import HttpException from "../exceptions/http-exception";
import { usersList } from "../types/types";

// Middleware for logging requests to txt file
export const appendLogToFile = (path: string,level: string) => async (req: Request,res: Response ,next: NextFunction)=> {
    await fs.appendFile(path, `${req.method}:: log from ${level}. Request url:  ${ req.url }. Request ID: ${req.requestID}. Time: ${Math.floor(Date.now()/1000)}\n`);
    next();
};

// Middleware for logging errors to txt file
export const appendToErrorLogger = (path: string) => async(err: HttpException, req: Request,res: Response ,next: NextFunction)=> {
    await fs.appendFile(path, `${err.status || 500}:: Message: ${err.message}. Request ID: ${req.requestID}. Time: ${Math.floor(Date.now()/1000)} >> ${ err.stack }.\n`);
    next(err);
};

// save users-list to JSON file
export async function save(path: string, usersList: usersList): Promise<void> {
    await fs.writeFile(path, JSON.stringify(usersList, null, 2));
}

// load users-list from JSON file
export async function load(path: string): Promise<string> {
    const json = await fs.readFile(path, "utf-8");
    return json;
}

// check if a file exists, if not - create one
export async function isFileExists(path: string): Promise<void> {
    try {
        await fs.access(path);
    } catch {
        await fs.writeFile(path, JSON.stringify([]), "utf-8");
    }
}
