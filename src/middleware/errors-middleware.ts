import { NextFunction, Request, Response } from "express";
import HttpException from "../exceptions/http-exception.js";
import UrlNotFoundException from "../exceptions/urlNotFound-exception.js";
import { IErrorResponse } from "../types/types.js";

// Middleware to response with error response object
export const responseWithError = (err: HttpException, req: Request , res: Response , next: NextFunction)=> {
    const errorResponse: IErrorResponse = { 
        status: err.status || 500, 
        message: err.message || 'Something went wrong'
    };
    res.status(errorResponse.status).json(errorResponse);
}

// Middleware to handle UrlNotFound Exception
export const urlNotFoundHandler = (req: Request , res: Response , next: NextFunction)=> {
    next(new UrlNotFoundException(req.path));
}