import { NextFunction, Request, Response } from "express";
import { genarateID } from "../utils/utils.strings.js";

// Middleware to generate requestID
export const generateRequestID = (req: Request, res: Response , next: NextFunction )=> {
    req.requestID = genarateID();
    next();
}
