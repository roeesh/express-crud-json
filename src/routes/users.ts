import express, { NextFunction, Request, Response } from 'express';
import { IResponseMessage, IUser } from '../types/types.js';
// import { isFileExists, load } from '../database/data-access.js';
import { idExist } from '../controller/user-validator.js';
import { createNewUser, deleteUser, readUser, updateUser } from '../controller/user-controller.js';
import usersModel from '../database/users-model.js';
import HttpException from '../exceptions/http-exception.js';

// const { JsonPATH } = process.env;
const router = express.Router();

// let usersLst: usersList;

// middleware to get the users-list if exists
const readUsersList =  async (req: Request, res: Response, next: NextFunction)=> {
    await usersModel.load();
    req.usersLst = usersModel.usersLst;
    next()
    // await isFileExists(JsonPATH as string);
    // const json = await load(JsonPATH as string);
    // usersLst = JSON.parse(json);
};

router.use(readUsersList);
router.use(express.json());

// Return a list of all existing users as JSON
router.get('/', async (req: Request,res: Response, next: NextFunction)=> {
    try {
        const outputResponse: IResponseMessage = {
            status: 200, 
            message: "All existing users",
            data: req.usersLst
        }
        res.status(outputResponse.status).json(outputResponse);
    } catch(err) {
        next(err);
    }
});

// Create new user
router.post('/',async (req: Request,res: Response, next: NextFunction)=> {
    try {
        const newUser: IUser = await createNewUser(req.usersLst,req.body as IUser);
        const outputResponse: IResponseMessage = {
            status: 200, 
            message: "User created",
            data: newUser
        }
        res.status(outputResponse.status).json(outputResponse);
    } catch(err) {
        next(err);
    }
});

// Read a single user using the id
router.get('/:userID', async (req: Request,res: Response, next: NextFunction)=> {
    try {
        if(idExist(req.usersLst,req.params.userID) === true) {
            const returnedUser = readUser(req.usersLst,req.params.userID);
            const outputResponse: IResponseMessage = {
                status: 200, 
                message: "User found",
                data: returnedUser
            }
            res.status(outputResponse.status).json(outputResponse);
        } else {
            throw new HttpException(400, `No such user with id: ${req.params.userID}`);
        }
    } catch(err) {
        next(err);
    }
});

// Update a user using the id
router.put('/:userID', async (req: Request,res: Response, next: NextFunction)=> {
    try {
        if(idExist(req.usersLst,req.params.userID) === true) {
            await updateUser(req.usersLst,req.params.userID,req.body as IUser);
            const userIndex = req.usersLst.findIndex((user) => user.id == req.params.id);
            const updatedUser = { ...req.usersLst[userIndex], ...req.body };
            const outputResponse: IResponseMessage = {
                status: 200, 
                message: "User updated",
                data: updatedUser
            }
            res.status(outputResponse.status).json(outputResponse);
            res.status(200).send(`Updated the user by the id of: ${req.params.userID}.`);
        } else {
            throw new HttpException(400, `No such user with id: ${req.params.userID}`);
        }
    } catch(err) {
        next(err);
    }
});

// Delete a user using the id
router.delete('/:userID', async (req: Request,res: Response, next: NextFunction)=> {
    try {
        if(idExist(req.usersLst,req.params.userID) === true) {
            await deleteUser(req.usersLst, req.params.userID);
            const outputResponse: IResponseMessage = {
                status: 200, 
                message: "User deleted",
                data: `id: ${req.params.userID}`
            }
            res.status(outputResponse.status).json(outputResponse);
        } else {
            throw new HttpException(400, `No such user with id: ${req.params.userID}`);
        }
    } catch(err){
        next(err);
    }
});

export default router;