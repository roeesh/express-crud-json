import express from 'express';

const router = express.Router();

router.get('/', (req, res) => {
    res.status(200).send(`Hello!\nPlease enter your name in the url.`)
});

//http://localhost:3030/name/Roee
//return html markup response
router.get('/name/:name', (req, res) => {
    const markup = `<h1>Hello ${req.params.name}</h1>
                    <p>Choose the action you want to do:</p>
                    <ul>
                        <li>Create user</li>
                        <li>Update user</li>  
                        <li>Delete user</li>  
                        <li>Read single user</li>  
                        <li>Show the list of all users</li>  
                    </ul>`
    res.status(200).set('Content-Type', 'text/html').send(markup)
});

export default router;