import express from 'express';
import usersRouter from '../routes/users.js'

const router = express.Router();

router.use('/users', usersRouter);

router.get('/',  (req, res) => {
    res.status(200).send('Hello api!')
});

export default router;