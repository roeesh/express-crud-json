import { usersList } from "../types/types";

// Checks if the given ID exits in users-list
export function idExist(usersLst: usersList, inputID: string): boolean {
    const returnedUser = usersLst.find((user)=> user.id === inputID);
    if(returnedUser !== undefined){
        return true;
    } else {
        return false;
    }
}

// Checks if the given username exists in users-list
export function usernameExist(usersLst: usersList, inputUsername: string): boolean {
    const returnedUser = usersLst.find((user)=> user.username === inputUsername);
    if(returnedUser !== undefined){
        return true;
    } else {
        return false;
    }
}