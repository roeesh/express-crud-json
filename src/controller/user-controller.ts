// import { save } from "../database/data-access.js";
import HttpException from "../exceptions/http-exception.js";
import { IUser, usersList } from "../types/types.js";
import { genarateID } from "../utils/utils.strings.js";
import { usernameExist } from "./user-validator.js";
import usersModel from '../database/users-model.js';


// const { JsonPATH } = process.env;

// Create new user
export async function createNewUser(usersLst: usersList, reqBody: IUser): Promise<IUser>{
    const {username, firstName, lastName, email, phone} = reqBody;
    if(!username || !firstName || !lastName || !email || !phone) { // Checks if got all User parameters
        throw new HttpException(421,"Not a valid User. Try again.");
    }
    if(usernameExist(usersLst,username) === true) { // Checks if username already exists
        throw new HttpException(409,`username: ${username} already exists. Try another one.`);
    }
    const newUser: IUser = {
        id: genarateID(),
        username,
        firstName,
        lastName,
        email,
        phone
    }
    usersLst.push(newUser);
    await usersModel.save(usersLst);
    return newUser;
    // await save(JsonPATH as string,usersLst);
}

// Read a single user
export function readUser(usersLst: usersList, idToRead: string) : IUser {
    const returnedUser = usersLst.find((user)=> user.id === idToRead);
    return returnedUser as IUser;
}

// Update a user using the id
export async function updateUser(usersLst: usersList, idToUpdate: string, reqBody: IUser) : Promise<void> {
    const newUsersLst: usersList = usersLst.map((user)=>{
        if (user.id === idToUpdate) {
            return {...user,
                username: reqBody.username,
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                phone: reqBody.phone
            };
        } else {
            return user;
        }
    });
    await usersModel.save(newUsersLst);
    // await save(JsonPATH as string,newUsersLst);
}

// Delete a user using the id
export async function deleteUser(usersLst: usersList, idToDelete: string): Promise<void> {
    const newUsersLst: usersList = usersLst.filter((user)=> user.id !== idToDelete);
    await usersModel.save(newUsersLst);
    // await save(JsonPATH as string,newUsersLst);
}