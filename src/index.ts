import express from 'express';
import { errorLogger, logger } from './utils/utils.cli.js';
import { appendLogToFile, appendToErrorLogger } from './database/data-access.js';
import { generateRequestID } from './middleware/requestId.js';
import { responseWithError, urlNotFoundHandler } from './middleware/errors-middleware.js';
import usersModel from './database/users-model.js';
import log from '@ajar/marker';

import webRouter from './routes/web.js';
import apiRouter from './routes/api.js';

const { PORT , HOST = "localhost" } = process.env;
const { LogPATH, ErrorPATH, JsonPATH } = process.env;
const LEVEL = "app level";

const app = express();

// Middlewares
app.use( generateRequestID ); //generate ID for each request
app.use( logger(LEVEL) ); //log to cli
app.use( appendLogToFile(LogPATH as string,LEVEL) ); //log to users.log file 
app.use(express.json());

// Connect routers
app.use('/', webRouter);
app.use('/api', apiRouter);

// Error handlers
app.use( urlNotFoundHandler );
app.use( appendToErrorLogger(ErrorPATH as string) );
app.use( errorLogger );
app.use( responseWithError );

// Application startup
app.listen(Number(PORT), HOST, async ()=> {
    await usersModel.init(JsonPATH as string);
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});



// default- return error: url not found (404 status with a custom response to unsupported routes)
// app.use((req: Request, res: Response, next: NextFunction) => {
//     next(new Error(`url: ${req.url} not found`));
//     // const full_url = new URL(req.url, `http://${req.headers.host}`);
//     // res.status(404).send(` - 404 - url ${full_url.href} was not found`)
// });

//// old handler(basic)
// app.use(appendToErrorLogger(ErrorPATH as string),errorLogger,(err: Error, req: Request, res: Response, next: NextFunction)=> {
//     res.status(400).json({
//         message: err.message,
//         stack: err.stack
//     });
// });



