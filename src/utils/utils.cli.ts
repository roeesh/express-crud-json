import { NextFunction, Request, Response } from "express"
import log from '@ajar/marker';
import HttpException from "../exceptions/http-exception.js";


// Logger middleware to cli
export const logger = (level: string) => (req: Request,res: Response ,next: NextFunction)=> {
    log.info(`${req.method}:: log from ${level}. Request url:  ${ req.url }. Time: ${Math.floor(Date.now()/1000)}`)
    next();
}

// Error logger middleware to cli
export const errorLogger = (err: HttpException, req: Request,res: Response ,next: NextFunction)=> {
    log.red(`${err.status || 500}:: Message: ${err.message}. Request ID: ${req.requestID}. Time: ${Math.floor(Date.now()/1000)}.`)
    next(err);
}
