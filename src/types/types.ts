export interface IUser {
    id: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string,
    phone: string
}

export interface IResponseMessage {
    status: number;
    message: string;
    data: any;
  }
export interface IErrorResponse {
    status: number;
    message: string;
    stack?: string;
}

export type usersList = IUser[];
